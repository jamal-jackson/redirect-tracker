## Setup

```bash
$ npm install
```

## Usage

Start MongoDB instance on port 27017

```bash
$ mongod
```

Start development server:

```bash
$ npm run dev
```

Start production server:

```bash
$ NODE_ENV=production npm run build && npm start
```
