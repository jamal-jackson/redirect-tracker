import redirectModel from './redirect.schema';

var redirectService = {};


redirectService.incrementRedirect = function(req, res) {
    // console.log(req.params.url);
    if (!isValidUrl(req.params.url)) return res.status(500).json({ error: "Please enter a valid URL" });
    redirectModel.updateRedirect(req.params.url, (error, record) => {
        if (error) return res.status(500).json({ error: error, message: "There was an issue handling your redirect. Please try again at a later time." })
        return res.redirect('http://' + req.params.url);
    })


    function isValidUrl(url) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return pattern.test(url);
    }
}

redirectService.getMostUsed = function(req, res) {
    redirectModel.getMostUsed(3, (error, records) => {
        if (error || !records) return res.status(500).json({ error: "There was an issue finding redirect records" });
        return res.json(records);
    })
};

export default redirectService;