import mongoose from 'mongoose';

const Schema = mongoose.Schema;

var redirectSchema = new Schema({
    url: { type: String, required: true },
    count: { type: Number, default: 1 }
}, {
    timestamps: true
});

// Methods
redirectSchema.statics.updateRedirect = function(url, cb) {
    console.log(url, 'its here');
    this.model('redirect').findOne({ url: url }, (error, record) => {
        if (error) return cb(error);
        if (!record) return this.addRedirect(url, cb);
        record.count++;
        return record.save(cb);
    });
};

redirectSchema.statics.addRedirect = function(url, cb) {
    let redirect = new this({
        url: url
    });
    return redirect.save(cb);
};

redirectSchema.statics.getMostUsed = function(limit, cb) {
    return this.model('redirect').find({}).sort('-count').limit(3).exec(cb);
}


export default mongoose.model('redirect', redirectSchema);