import express from 'express';
import redirectService from './redirect.service';
var router = express.Router();


router.get('/:url', redirectService.incrementRedirect);
router.get('/', redirectService.getMostUsed);

export default router;