import express from 'express';
import redirectRoutes from './redirect/index';
var apiRoutes = express.Router();

apiRoutes.use('/redirect', redirectRoutes);

export default apiRoutes;