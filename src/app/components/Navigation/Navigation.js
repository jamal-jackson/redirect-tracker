import React from 'react';
import { NavLink } from 'react-router-dom';

import style from './Navigation.css';

function Navigation() {
  return (
    <nav className="row">
      <NavLink
        className={`col-1`}
        activeClassName={style.activeLink}
        to="/"
        exact
      >
        Home
      </NavLink>
      <NavLink
        className={`col-1`}
        activeClassName={style.activeLink}
        to="/about"
        exact
      >
        About
      </NavLink>
    </nav>
  );
}

export default Navigation;
