import React from 'react';

function About() {
  return (
    <div>
      <h3 className="row">What Is this Project</h3>
      <div className="row">
      <p>
        This is a code test for tracking redirects called on this endpoint, <b>/api/redirect/:url</b>
      </p>
      <p>
        All successful redirects are then tracked and stored in a MongoDB collection, and the top 3 redirects are reported on the home page
      </p>
      </div>
    </div>
  );
}

export default About;
