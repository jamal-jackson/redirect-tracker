import React, { Component, PropTypes } from 'react';
import axios from 'axios';
import './Home.css';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirects: [],
      redirectError: ''
    };
    this.redirectList;
  }

  componentDidMount(){
    axios.get('/api/redirect')
    .then(
      res => {
        this.redirectList = res.data.map(redirect =>
            <div className="col-12 row redirect" key={redirect.url}>
            <p className="col-sm-12 col-md-3">
              <b>URL: </b> {redirect.url}
            </p>
            <p className="col-sm-12 col-md-3">
              <b>Times Redirected: </b> {redirect.count}
            </p>
            </div>
          );
          this.setState({redirects: res.data});
      },
      error => {
        let errorMessage = 
          <div className="col-12 row">
            <p>{error.response.data.error}</p>
          </div>;
        this.setState({redirectError: errorMessage})
      }
    )
  }

  render(){
    return (
      <article>
        <div className="row">
          <h3 className="col-12">Most Frequent Redirects</h3>
        </div>
        <div className="row redirects">
          {this.redirectList}
        </div>
        {this.state.redirectError}
      </article>
    );
  }
}

export default Home;
